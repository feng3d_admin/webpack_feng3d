import * as feng3d from "feng3d";

window.onload = function () {
    var engine = new feng3d.Engine();
    var cube = feng3d.gameObjectFactory.createCube();
    cube.transform.z = 4;
    cube.transform.y = -1;
    engine.scene.gameObject.addChild(cube);

    feng3d.ticker.onframe(() => {
        cube.transform.ry++;
    });
}